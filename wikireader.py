import json
import argparse
from sseclient import SSEClient as EventSource
from google.cloud import pubsub_v1


STREAM_URL = 'https://stream.wikimedia.org/v2/stream/recentchange'


def get_callback(api_future, data, ref):
    def callback(api_future):
        try:
            print("Published message now has ID {}".format(api_future.result()))
            ref["num_messages"] += 1
        except Exception:
            print("A problem occurred when publishing {}: {}\n".format(data, api_future.exception()))
            raise
    return callback


def pub(project_id, topic_name):
    ref = dict({"num_messages": 0})
    client = pubsub_v1.PublisherClient()
    topic_path = client.topic_path(project_id, topic_name)
    for event in EventSource(STREAM_URL):
        if event.event == 'message':
            try:
                r_event = json.loads(event.data, encoding='utf-8')
            except ValueError:
                print("Broken event, skipping")
                continue
            msg = event.data.encode(encoding='utf-8')
            api_future = client.publish(topic_path, data=msg)
            api_future.add_done_callback(get_callback(api_future, msg, ref))


if __name__ == "__main__":
    """
    export GOOGLE_APPLICATION_CREDENTIALS=/Users/lancer/PycharmProjects/cloud_workshop/key.json
    export PROJECT=`gcloud config get-value project`
    python wikireader.py $PROJECT hello-topic
    """
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )
    parser.add_argument("project_id", help="Google Cloud project ID")
    parser.add_argument("topic_name", help="Pub/Sub topic name")

    args = parser.parse_args()

    pub1(args.project_id, args.topic_name)
